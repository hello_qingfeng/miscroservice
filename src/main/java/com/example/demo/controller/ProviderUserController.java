package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.UserDao;
import com.example.demo.entity.User;

@RestController
public class ProviderUserController {
	@Autowired
	private UserDao userdao;
	@GetMapping("/getUser/{id}")
	public User getUserById(@PathVariable Long id) {
		User one = userdao.getOne(id);
		System.out.println(one);
		return userdao.getOne(id);
	}
}
