package com.example.demo.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 自定义默认全局异常捕捉处理器
 * 
 * @author peixinxu
 *
 */
@ControllerAdvice
public class GlobalDefaultException {
	
	/**
	 * 设置捕捉的异常，并做处理
	 * @param req
	 * @param e
	 */
	@ExceptionHandler(value=Exception.class)
	public void delfaultErrorHandler(HttpServletRequest req,Exception e) {
		System.out.println("进入自定义异常。。。");
	}
}
